//
//  CORIdfaConsentView.h
//  Unity-iPhone
//
//  Created by atarin on 27.03.2024.
//
//

#import <Foundation/Foundation.h>

typedef void (*IdfaStatus)(int status);

@interface CORIdfaConsentView : NSObject

+ (CORIdfaConsentView*)sharedManager;
- (void)addNotificationObserverWith:(IdfaStatus) callback;
- (void)waitForApplicationDidBecomeActiveToShowAttWith:(IdfaStatus) callback;
- (void)showIdfaConsentView:(IdfaStatus) callback;

@end

