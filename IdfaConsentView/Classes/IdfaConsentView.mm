#if defined(__IPHONE_14_0)
#import <AppTrackingTransparency/ATTrackingManager.h>
#endif
#import <AdSupport/AdSupport.h>
#import "CORIdfaConsentView.h"

extern "C" {

    bool CanAskForTrackingConsent() {
        
#if defined(__IPHONE_14_0)
        if(@available(iOS 14.0, macOS 11.0, tvOS 14.0, *)) {
            
            if ([NSProcessInfo processInfo].isiOSAppOnMac)
            {
                return false;
            }
            
            ATTrackingManagerAuthorizationStatus status = [ATTrackingManager trackingAuthorizationStatus];
           
            switch (status) {
                case ATTrackingManagerAuthorizationStatusNotDetermined: return true;
                case ATTrackingManagerAuthorizationStatusRestricted: return false;
                case ATTrackingManagerAuthorizationStatusDenied: return false;
                case ATTrackingManagerAuthorizationStatusAuthorized: return false;
                default: return false;
            }
        }
        else
        {
#endif
            return false;
#if defined(__IPHONE_14_0)
        }
#endif
    }

    void ShowNativeIdfaPopup(IdfaStatus callback) {
        [[CORIdfaConsentView sharedManager] showIdfaConsentView: callback];
    }
}
