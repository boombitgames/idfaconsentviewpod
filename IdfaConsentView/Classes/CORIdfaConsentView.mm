//
//  CORIdfaConsentView.m
//  Unity-iPhone
//
//  Created by atarin on 27.03.2024.
//
//

#import "CORIdfaConsentView.h"
#import <AppTrackingTransparency/ATTrackingManager.h>

@implementation CORIdfaConsentView

IdfaStatus idfaCallback = nil;

+ (CORIdfaConsentView*)sharedManager
{
	static CORIdfaConsentView *sharedSingleton;
	
	if( !sharedSingleton )
    {
		sharedSingleton = [[CORIdfaConsentView alloc] init];
    }
    
	return sharedSingleton;
}

- (void)addNotificationObserverWith:(IdfaStatus) callback;
{
    idfaCallback = callback;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(waitForApplicationDidBecomeActiveNotification )
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)waitForApplicationDidBecomeActiveToShowAttWith:(IdfaStatus) callback;
{
    idfaCallback = callback;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(waitForApplicationDidBecomeActiveToShowAtt )
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
}

-(void)waitForApplicationDidBecomeActiveNotification
{
    if (@available(iOS 14, *)) {
        ATTrackingManagerAuthorizationStatus status = [ATTrackingManager trackingAuthorizationStatus];
        
        int universalStatus = 1;
        switch (status) 
        {
            case ATTrackingManagerAuthorizationStatusNotDetermined:
                universalStatus = 0;
                break;
            case ATTrackingManagerAuthorizationStatusRestricted:
                universalStatus = 1;
                break;
            case ATTrackingManagerAuthorizationStatusDenied:
                universalStatus = 2;
                break;
            case ATTrackingManagerAuthorizationStatusAuthorized:
                universalStatus = 3;
                break;
            default:
                universalStatus = 1;
                break;
        }
        
        idfaCallback(universalStatus);
        
    } 
    else
    {
        idfaCallback(-1);
    }
    idfaCallback = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
}

-(void)waitForApplicationDidBecomeActiveToShowAtt
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
    [[CORIdfaConsentView sharedManager] showIdfaConsentView: idfaCallback];
}

- (void)showIdfaConsentView:(IdfaStatus) callback {
#if defined(__IPHONE_14_0)
    if(@available(iOS 14.0,*)) {
        if (UIApplication.sharedApplication.applicationState != UIApplicationStateActive)
        {
            [[CORIdfaConsentView sharedManager] waitForApplicationDidBecomeActiveToShowAttWith: callback];
            return;
        }
        
        [ATTrackingManager requestTrackingAuthorizationWithCompletionHandler:^void (ATTrackingManagerAuthorizationStatus status) {
            
            int universalStatus = 1;    // Denied
            switch (status) {
                case ATTrackingManagerAuthorizationStatusNotDetermined:
                    universalStatus = 0;
                    break;
                case ATTrackingManagerAuthorizationStatusRestricted:
                    universalStatus = 1;
                    break;
                case ATTrackingManagerAuthorizationStatusDenied:
                    
                    if ([ATTrackingManager trackingAuthorizationStatus] == ATTrackingManagerAuthorizationStatusNotDetermined)
                    {
                        [[CORIdfaConsentView sharedManager] addNotificationObserverWith: callback];
                        return;
                    }
                    
                    universalStatus = 2;
                    break;
                case ATTrackingManagerAuthorizationStatusAuthorized:
                    universalStatus = 3;
                    break;
                default:
                    universalStatus = 1;
                    break;
            }
            callback(universalStatus);
        }];
    }
    else
    {
#endif
        callback(-1);
#if defined(__IPHONE_14_0)
    }
#endif
}


@end
